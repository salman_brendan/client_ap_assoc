import os
import xml.etree.ElementTree
import psycopg2

path='/root/client_ap_assoc/Cent_DBConst/amp_data/AMP'
path2='/root/client_ap_assoc/Cent_DBConst/amp_data/RAMP'

try: 
	conn = psycopg2.connect(dbname= 'amp_parseql', user='postgres',password='password')
except: 
	print "I can't connect"
 
rmacs1 = {}
rmacs2 = {}
cur1 = conn.cursor()
cur2 = conn.cursor()	

f_rm1 = open('./rm1.txt','w')
f_rm2 = open('./rm2.txt','w')

for filename in os.listdir(path):
	if 'ap_list' in filename: 
		fullname=os.path.join(path,filename)
		e = xml.etree.ElementTree.parse(fullname).getroot()
		if e!=None: 
			for atype in e.findall('ap'): 
				ap_name=atype.find('name').text
				if atype.find('radio') != None: 
					for radio_macs in atype.findall('radio'):
						ind = radio_macs.get('index')
						if ind == '1':
							rmacs1[ap_name]=radio_macs.find('radio_mac').text
						else: 
							rmacs2[ap_name]=radio_macs.find('radio_mac').text

'''
produced textual output for parsing trouble shooting etc
for key in rmacs1: 
	f_rm1.write(key+', '+rmacs1[key]+'\n')

for key in rmacs2: 
	f_rm2.write(key+', '+rmacs2[key]+'\n')
'''

for key in rmacs1: 
	
	cur1.execute("update sess_trace.ap set radio_mac1 = %s where apid like %s;",
	(rmacs1[key],key))
	
for key in rmacs2: 
	
	cur2.execute("update sess_trace.ap set radio_mac2 = %s where apid like %s;",
	(rmacs2[key],key))
	

conn.commit()
conn.close()

	#also want to write part of the script to dump into a text file
	#so that way we can spot check radio macs etc.
	
														 
								 

