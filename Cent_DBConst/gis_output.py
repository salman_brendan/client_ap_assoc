import os
from datetime import datetime
import string
import psycopg2
import json
import csv

#script to produce csv or json format to use as a layer on gis. accepts mac, start and stop time to retunr a string of points

def ret_output(mac, start, stop):

#mac input should be starndard, start and stop time should be in format "YYYY/MM/DD 24hh:mi:ss"


	try: 
		conn = psycopg2.connect("dbname = 'amp_parseql' user='postgres' host='localhost' password='password'")
	except: 
		print "I can't connect."

	cur = conn.cursor()

	cur.execute("select lat, long, start from sess_trace.building, sess_trace.sessions where (sess_trace.building.fold, sess_trace.sessions.start) in (select sess_trace.ap.folder, sess_trace.sessions.start from sess_trace.ap, sess_trace.sessions where sess_trace.sessions.apid = sess_trace.ap.apid AND sess_trace.ap.apid in (SELECT apid from sess_trace.sessions WHERE mac=%s and ((to_timestamp(substring(start from 0 for 11)||' '||substring(start from 12 for 8),'YYYY-MM-DD hh24:mi:ss') > to_timestamp(%s,'YYYY-MM-DD hh24:mi:ss') and to_timestamp(substring(stop from 0 for 11)||' '||substring(stop from 12 for 8),'YYYY-MM-DD hh24:mi:ss') < to_timestamp(%s,'YYYY-MM-DD hh24:mi:ss')) or (to_timestamp(substring(start from 0 for 11)||' '||substring(start from 12 for 8),'YYYY-MM-DD hh24:mi:ss') < to_timestamp(%s,'YYYY-MM-DD hh24:mi:ss') and to_timestamp(substring(stop from 0 for 11)||' '||substring(stop from 12 for 8),'YYYY-MM-DD hh24:mi:ss') < to_timestamp(%s,'YYYY-MM-DD hh24:mi:ss')))) AND (to_timestamp(substring(sess_trace.sessions.start from 0 for 11)||' '||substring(sess_trace.sessions.start from 12 for 8),'YYYY-MM-DD hh24:mi:ss') > to_timestamp(%s,'YYYY-MM-DD hh24:mi:ss') and to_timestamp(substring(sess_trace.sessions.stop from 0 for 11)||' '||substring(sess_trace.sessions.stop from 12 for 8),'YYYY-MM-DD hh24:mi:ss') < to_timestamp(%s,'YYYY-MM-DD hh24:mi:ss')));",(mac, start, stop, start, stop, start, stop))

	output =  cur.fetchall()
	print output[0][2]
	conn.commit()
	conn.close()
	
	ds = {}
	y = 1	
	
	
	for x in range(0, len(output)-1):
		ms = {}
		key = 'pt'+str(x+1)
		ms['x'] = output[x][1]
		ms['y'] = output[x][0]
		ms['time'] = output[x][2]
		ds[key] = ms

	print ds

	with open('ret_data.json','wb') as f:
		json.dump(ds, f)
	
	#create csv file
	with open('ret_output.csv','wb') as csvfile: 
		test = csv.writer(csvfile,delimiter=',')
		test.writerow(['lat','long','Date'])
		for x in range(0, len(output)-1):
			dt = output[x][2]
			dt_s = dt[0:10]
			t_s = dt[11:19]
			print dt_s
			print t_s 
			test.writerow([output[x][0],output[x][1],t_s+' '+dt_s])

	
################################################################################################
#main


ret_output('F8:A9:D0:1A:F5:71','2016-02-18 14:29:30','2016-02-23 00:00:00')

 

