import XMLParser
import shutil 
import glob
import fnmatch
import os
from datetime import datetime
import string
import psycopg2

master_dict = {}

#prinnt time for reference
print datetime.now().time()

#purge directory of empty files
XMLParser.file_purge('/root/client_ap_assoc/Cent_DBConst/amp_data/AMP/client_detail')
XMLParser.file_purge('/root/client_ap_assoc/Cent_DBConst/amp_data/RAMP/client_detail')

#parse both AMP and ramp client data 
XMLParser.parse_file('/root/client_ap_assoc/Cent_DBConst/amp_data/AMP/client_detail',master_dict)
XMLParser.parse_file('/root/client_ap_assoc/Cent_DBConst/amp_data/RAMP/client_detail',master_dict)

#print time again for reference
print datetime.now().time()

f1 = open('./mark.txt','w+')
mark_col = [] 
for keys in master_dict:  
		test_str = master_dict[keys][2]
		if str.find(test_str, '-',0, len(test_str)-1)==-1:
			mark_col.append('und')
		else: 
			ind = test_str.index('-')
			folder = test_str[0:ind]
			if folder in mark_col: 
				mark_col = mark_col
			else:
				mark_col.append(folder)

for items in mark_col: 
	f1.write(items+'\n')

f1.close()


#establish connection with database
try: 
	conn = psycopg2.connect(user='postgres', database='amp_parseql', password='password')
#ubuntu syntax: "dbname = 'amp_parseql' user='postgres' host='localhost' password='password'"
except: 
	print "I can't connect."

cur = conn.cursor()

#execute command using postgres cursor and print results after determining if ip addresses were assigned to the client, the queries insert data into tables if the key is not already in the database

for key in master_dict:
	if len(master_dict[key][6])>0:
		if len(master_dict[key][6])<2:
			cur.execute("insert into sess_trace.sessions(id, asso_id, mac, uname, apid, start, stop,ipaddr1,ipaddr2) select %s,%s,%s,%s,%s,%s,%s,%s,%s where not exists(select id from sess_trace.sessions where id = %s);",(key,master_dict[key][1],master_dict[key][0],master_dict[key][5],master_dict[key][2],master_dict[key][3],master_dict[key][4],master_dict[key][6][0],'\0',key))

		else:
			cur.execute("insert into sess_trace.sessions(id, asso_id, mac, uname, apid, start, stop,ipaddr1,ipaddr2) select %s,%s,%s,%s,%s,%s,%s,%s,%s where not exists(select id from sess_trace.sessions where id = %s);",(key,master_dict[key][1],master_dict[key][0],master_dict[key][5],master_dict[key][2],master_dict[key][3],master_dict[key][4],master_dict[key][6][0],master_dict[key][6][1],key))
	else: 
		cur.execute("insert into sess_trace.sessions(id, asso_id, mac, uname, apid, start, stop,ipaddr1,ipaddr2) select %s,%s,%s,%s,%s,%s,%s,%s,%s where not exists(select id from sess_trace.sessions where id = %s);",(key,master_dict[key][1],master_dict[key][0],master_dict[key][5],master_dict[key][2],master_dict[key][3],master_dict[key][4],'\0','\0',key))

cur.execute("select * from sess_trace.sessions where id = 'F8:A9:D0:1A:F5:71_2016-03-09T11:48:01-05:00_EGGE-313DA1132T';")

print cur.fetchone()

#commit connection then close
conn.commit()
conn.close()

#print time again for reference
print datetime.now().time()

