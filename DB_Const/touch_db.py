import os 
import xml.etree.ElementTree
import psycopg2
path='/home/bmattina/client_ap_assoc/amp_data/AMP'
path2 = '/home/bmattina/client_ap_assoc/amp_data/RAMP'
global conn
serial_num_list=[]

def xml_parser(path):

	folder_collec = [];
	cur = conn.cursor()
 
	for filename in os.listdir(path):
		if 'ap_list' in filename:
		    fullname=os.path.join(path,filename)
		    e = xml.etree.ElementTree.parse(fullname).getroot()
		    if e!=None:	
                       for atype in e.findall('ap'):
         		   ap_id=atype.get('id')
                           device_category=atype.find('device_category').text 
                           ##Check if device is of type other category, then serial number does not exist
                           if atype.find('serial_number') != None:
                              ser_num=atype.find('serial_number').text
                           else:
                              ser_num=0
			   try:
                           	lan_mac=atype.find('lan_mac').text
			   except:
				lan_mac = '\0'
                           ap_name=atype.find('name').text
                           ##Check if device is access_point only then controller_id exists like switch,other. sometimes data is missing"
                           if atype.find('controller_id') != None: 
                              controller_id=atype.find('controller_id').text
                           else:
                              controller_id=0

                  	   print(lan_mac,ap_name,device_category,ser_num,ap_id,controller_id)
			 #cut apid from ap_name
			   if str.find(ap_name, '-',0, len(ap_name)-1)==-1:
				folder = 'und'
			   else: 
                           	ind = ap_name.index('-')
			   	folder = ap_name[0:ind]
			   #entering the above values into the ap table of the data base.
			   
			   if folder in folder_collec: 
				folder_collec = folder_collec	
			   else: 
			   	folder_collec.append(folder)
		  			
               		   cur.execute("insert into sess_trace.ap(apid, lan_mac, folder, floor, lat, long) select %s, %s, %s, %s,%s,%s where not exists(select apid from sess_trace.ap where apid = %s);",(ap_name, lan_mac, folder,0,'\0','\0', ap_name))
				
                           #Adding support for the radio mac's might be needed in the future
                           print "These are the radio mac's" 
                           for radio_mac in atype.findall('radio'):
                               radio_index=radio_mac.get('index')                             
                               radio_mac=radio_mac.find('radio_mac').text               
                               print (radio_index,radio_mac)                               
	return folder_collec;

try:
	conn = psycopg2.connect("dbname = 'amp_parseql' user='postgres' host='localhost' password='password'")

except:
	print "I can't connect"

fc1 = xml_parser(path)
fc2 = xml_parser(path2)
conn.commit()
conn.close() 

f1 = open('./col_out.txt','w+')

for items in fc1: 
	f1.write(items+'\n')

for items in fc2:
	f1. write(items+'\n')

f1.close()      			                                                                                                                                          
