import os 
import xml.etree.ElementTree
import psycopg2
import string 

my_dict = {}

path='/home/bmattina/client_ap_assoc/amp_data/AMP'

path2='/home/bmattina/client_ap_assoc/amp_data/RAMP'


def xml_parser(path):
	for filename in os.listdir(path):
		if 'visualrf_building' in filename:
		    fullname=os.path.join(path,filename)
		    e = xml.etree.ElementTree.parse(fullname).getroot()
		    if e!=None:	
                       for atype in e.findall('building'):
         		   ap_name=atype.get('name')
                           ap_latitude=atype.get('latitude')
                           ap_longitude=atype.get('longitude')
                           ap_address=atype.find('address').text
                           height=atype.get('floor_height_ft')
                           
 			   if len(ap_latitude)>0:
			   	dat = []
				dat.append(ap_latitude)
				dat.append(ap_longitude)
				my_dict[ap_name]=dat		   

			   print(ap_name,ap_latitude,ap_longitude,ap_address,height)

#main
##################################################################


#open files to read current folder and building names
fb = open('./build.txt')
ff = open('./folder.txt')
fl = open('./lat_long.txt')

xml_parser(path)
xml_parser(path2)

#establish connection with database
try:
        conn = psycopg2.connect("dbname = 'amp_parseql' user='postgres' host='localhost' password='password'")
except:
        print "I can't connect."

cur = conn.cursor()

#build collections out of files for mark's journey. use these colle
fold_col = [];
build_col = [];
long_col = [];
lat_col = [];

for line in ff: 
	fold_col.append(line.strip())
	print line+'.'
for line in fb:
	build_col.append(line.strip())
	print line.strip()+'.'

for line in fl: 
	print line.strip()+'.'
	samp = line.strip()
	ind = samp.index('	')
	longi =	samp[0:ind].strip()
	lat = samp[ind+1:len(samp)-1].strip()
	print longi+'_'+lat+'\n' 
	lat_col.append(lat)
	long_col.append(longi)

for x in range(0, len(fold_col)-1):
 
	cur.execute("insert into sess_trace.building(fold,name, lat, long) select %s,%s,%s,%s where not exists(select fold from sess_trace.building where fold = %s)",(fold_col[x], build_col[x],lat_col[x],long_col[x], fold_col[x]))
	

#for key in my_dict:
	
#	if key in build_col: 
#		cur.execute("update sess_trace.building set lat = %s, long = %s where name = %s",(my_dict[key][0],my_dict[key][1],key))

conn.commit()
conn.close()



                                                                                                    
