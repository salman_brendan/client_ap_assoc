import os
import psycopg2
import string

 
#try:
#        conn=psycopg2.connect("dbname='amp_parseql' user='postgres' host='localhost'password='brendan'")

#except:
#	print "I am unable to connect"

#create handles to open the necessary files to compare information previously amassed and information that still needs to be compiled. 
f_amp = open('./amp_fold_list.txt')
f_mf = open('./mark_fold_list.txt')
f_mb = open('./mark_build_list.txt')
f_of = open('./off_fold_list.txt')
f_ob = open('./off_build_list.txt')
f_out = open('./dict_out.txt','w')
f_out2 = open('./col_out.txt','w')

amp_col = [];
mark2dict_col = [];
res_col =[];
mf_col = [];
mb_col = [];
mark_dict = {};
of_col = [];
ob_col = [];
off_dict = {};
ret_dict = {};

#create function to combine collections into a dictionary

def coll_to_dict(key_col, val_col, rdict):
	for x in range(0, len(key_col)-1):
		rdict[key_col[x]] =  val_col[x]

#create function to compare reference amp collection to dictionaries, record resultsand adjust reference collection if necessary

def col_dict_comp(ref_col,int_col, ref_dict, res_dict):
	
	for x in range(0, len(ref_col)-1):
		#check to determine if a folder value is already in ref dict as a kekey, if it is record key and value into the res dict
		try:
			if ref_col[x] in ref_dict: 
				res_dict[ref_col[x]] = ref_dict.get(ref_col[x])
				#instead of removing from collection which is screwing everything up, add to a new collection if it isn't recorded. ELIF condition tellsthe program to do nothing if the term is already in the intermediate collection.
			elif ref_col[x] in int_col:
				y = 0;
			else: 
				int_col.append(ref_col[x])
		except: 
			break   


#MAIN--------------------------------------------------------------------#

#create reference collection and dictionaries
for line in f_amp:
	amp_col.append(line.strip().lower())

#first list keys and then values in collections 
for line in f_mf:
	mf_col.append(line.strip().lower())
for line in f_mb:
	mb_col.append(line.strip().lower())

#off folds and builds into collections
for line in f_of:
	of_col.append(line.strip().lower())
for line in f_ob:
	ob_col.append(line.strip().lower())

#function to convert collections to dictionaries
coll_to_dict(mf_col,mb_col, mark_dict)
coll_to_dict(of_col, ob_col, off_dict)

#compare amp_col to both mark_dict and off_dict. Successful comparison populates the ret dict

#db
print len(amp_col)

#for marks data
col_dict_comp(amp_col,mark2dict_col, mark_dict, ret_dict)

#debug
print "first iteration" 
print "ret dict"
print sorted(ret_dict)
print "mark dict"
print sorted(mark_dict)
print "lengths: Mark, Ret"
print len(mark_dict)
print len(ret_dict)

#for official data
col_dict_comp(amp_col,res_col, off_dict, ret_dict)
print "second iteration"
print "lengths ret/off"
print len(ret_dict)
print len(off_dict)
print "ret dict"
print sorted(ret_dict)
print "check"
print len(res_col)+len(ret_dict)
print len(amp_col)

#need to print the outcome of the the ret_dict (what we gleaned), keys and values, as well as the res_col, which indicates the left overs.

for key in ret_dict:
	f_out.write(key+', '+ret_dict[key]+'\n')

for x in range(len(res_col)-1):
	 f_out2.write(res_col[x]+'\n')


