import os
import xml.etree.ElementTree as ET
import fnmatch
import string 
from datetime import datetime
import ipaddress
import yaml
import math

filename='./snort_map.xml'

'''
def xml_parser(filename):
	e= xml.etree.ElementTree.parse(filename).getroot()
	if e!=None:
'''

#create one temporary and one master dictionary to complete
#translation of min/max address to indiviadual keys for ip->building
#yaml translation
master_ip_dict = {}
build_ip_dict = {}

#parse ipr query to extract ip address ranges
tree = ET.parse(filename).getroot()
for subnets in tree.findall('subnets'):
	#for subnet in parsed data 
	for subnet in subnets:
		#get min/max address and description. also grab
		#router address if available, may be helpful 
		#later for room mapping etc. 
		min_addr=subnet[0].text
		max_addr=subnet[1].text
		prefix = subnet[2].text
		desc=subnet[4].text
		if len(subnet) > 5:
			router=subnet[5].text
		#build initia table with description as key and min/max as value 
		build_ip_dict[min_addr] = str(desc)+':'+prefix

#now run loop on temporary dictionary
for key in build_ip_dict:
	descrPrefStr = build_ip_dict[key]
	#split string by comma,including prefix range which will help you calculate
	#ip ranges 
	desc,prefix = descrPrefStr.split(':')
	min_ip = ipaddress.IPv4Address(key)
	pref_rng = pow(2,(32-int(prefix)))
	#print(pref_rng)	
	for x in range(0,pref_rng-1):
		ip_addr = min_ip+x
		master_ip_dict[str(ip_addr)] = desc

stream = file('ip_addr.yaml','w')
yaml.safe_dump(master_ip_dict,stream)

		
		
	


		

